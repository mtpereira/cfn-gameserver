#!/usr/bin/env python

import requests
import boto3

IMAGES_URL = 'https://cloud-images.ubuntu.com/releases/streams/v1/com.ubuntu.cloud:released:aws.json'

def validate_template(template_body):
    c = boto3.client('cloudformation')
    return c.validate_template(TemplateBody=template_body)

def filter_images(images, arch, version, storage, virt, regions):
    json = images.json()
    versions = json['products']['com.ubuntu.cloud:server:{}:{}'.format(version, arch)]['versions']
    latest_version = versions[versions.keys()[len(versions) - 1]]['items']
    result = {}
    for version in latest_version.itervalues():
        for region in regions:
            if version['root_store'] == storage and version['crsn'] == region and version['virt'] == virt:
                result.update({region: {'ami': version['id']}})
    return result

def get_latest_image(regions, arch='amd64', version='14.04', storage='ebs', virt='hvm'):
    fetch = requests.get(IMAGES_URL)
    if fetch.status_code != 200:
        raise Exception('Failed to fecth info on Ubuntu images from %s', IMAGES_URL)
    return filter_images(fetch, arch, version, storage, virt, regions)

def main():
    return get_latest_image(['eu-central-1'])

if __name__ == '__main__':
    print main()


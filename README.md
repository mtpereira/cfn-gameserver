Using Troposphere (https://github.com/cloudtools/troposphere) for generating a CloudFormation template.

Setup
=====

`pip install -r requirements.txt`

Generate CloudFormation template
================================

`python cfn.py > cfn.template`

Premisses and compromises
=========================

* Using Ubuntu images on the EC2 instances;
* Paravirtualization, EBS storage and 64-bit architecture are preferred;
* Assumes that there's already an S3 bucket created with the service's source
  stored in a .zip file. Also assumes that the bucket has a public read policy.

Current issues
==============

* The cfn-signal command returns a ValidationError, stating that the stack is
  already in the CREATE_COMPLETE state and, as such, doesn't signal the ASG
  that the instances are up. Therefor, the ASG will terminate and start
  intances on a loop.
* Missing logging to CloudWatch.


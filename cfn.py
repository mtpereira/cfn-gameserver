#!/usr/bin/env python

from troposphere import Template, Parameter, Ref, FindInMap, Output, GetAtt, \
    Select, Join, Tags, Base64, GetAZs
from troposphere.ec2 import SecurityGroup, SecurityGroupRule
from troposphere.s3 import Bucket, PublicRead
from troposphere.autoscaling import AutoScalingGroup, LaunchConfiguration, Metadata, ScalingPolicy
from troposphere.elasticloadbalancing import LoadBalancer, HealthCheck, ConnectionDrainingPolicy, AccessLoggingPolicy
from troposphere.cloudformation import Init, InitConfig, InitConfigSets, InitFiles, InitFile
from troposphere.cloudwatch import Alarm, MetricDimension
from troposphere.policies import CreationPolicy, ResourceSignal, UpdatePolicy, AutoScalingRollingUpdate
import utils

REGIONS = [
    'eu-west-1',
    'eu-central-1',
    'us-east-1',
    'us-west-1',
]

t = Template()

t.add_version('2010-09-09')

t.add_description(
    'CFN test.'
)

#
# Parameters
#

instancetype_param = t.add_parameter(Parameter(
    "InstanceType",
    Default = "t2.micro",
    ConstraintDescription = "Must be a valid EC2 instance type.",
    Type = "String",
    Description = "Gameserver EC2 instance type.",
))

asgscale_param = t.add_parameter(Parameter(
    "ASGScale",
    Default = "2",
    ConstraintDescription = "Must be a positive integer.",
    Type = "Number",
    Description = "Scale of the Auto Scaling Group.",
))

maxbandwidth_param = t.add_parameter(Parameter(
    "MaxBandwidth",
    Default = "104857600",
    ConstraintDescription = "Must be a positive integer.",
    Type = "Number",
    Description = "Maximum bandwidth for scale up, in bytes.",
))

bucket_param = t.add_parameter(Parameter(
    "SourceBucket",
    Default = "s3sourcegameserver",
    ConstraintDescription = "Must be a valid S3 bucket.",
    Type = "String",
    Description = "S3 bucket with the service's source.",
))

#
# Mappings
#

t.add_mapping("AMIPerRegion", utils.get_latest_image(REGIONS))

#
# Resources
#

install_cfn_init = [
    "#!/bin/bash\n",
    "\n",
    "sudo apt-get update\n",
    "sudo apt-get --no-install-recommends --yes install python-pip\n",
    "sudo pip install https://s3.amazonaws.com/cloudformation-examples/aws-cfn-bootstrap-latest.tar.gz\n",
    "/usr/local/bin/cfn-init --region '", Ref("AWS::Region"), "' ",
    "--stack '", Ref("AWS::StackId"), "' ",
    "--resource GameServerConfig\n",
    "\n",
   "/usr/local/bin/cfn-signal -e $? ",
    "--region ", Ref("AWS::Region"), " ",
    "--stack ", Ref("AWS::StackId"), " ",
    "--resource GameServerASG\n",
]

init_service = [
    "#!upstart\n",
    "\n",
    "description GameServer\n",
    "author root\n",
    "\n",
    "start on (local-filesystems and net-device-up)\n",
    "stop on runlevel [06]\n",
    "setuid ubuntu\n",
    "script\n",
    "exec /opt/gameserver/service.sh\n",
    "end script\n",
]

gameserver_loadbalancer = t.add_resource(LoadBalancer(
    "GameServerLoadBalancer",
    HealthCheck=HealthCheck(
        Target="HTTP:8080/",
        Timeout="10",
        Interval="30",
        UnhealthyThreshold="3",
        HealthyThreshold="2",
    ),
    Listeners=[{"InstancePort": "8080", "LoadBalancerPort": "8080", "Protocol": "TCP" }],
    AvailabilityZones=GetAZs(""),
))

gameserver_secgroup = t.add_resource(SecurityGroup(
    "GameServerSecurityGroup",
    GroupDescription = "Enable access to the service from ELB",
    SecurityGroupIngress = [
        SecurityGroupRule(
            IpProtocol = "tcp",
            FromPort = "8080",
            ToPort = "8080",
            SourceSecurityGroupOwnerId = GetAtt(gameserver_loadbalancer, "SourceSecurityGroup.OwnerAlias"),
            SourceSecurityGroupName = GetAtt(gameserver_loadbalancer, "SourceSecurityGroup.GroupName")
        )
    ]
))

gameserver_config = t.add_resource(LaunchConfiguration(
    "GameServerConfig",
    Metadata = Metadata(
        Init(
            InitConfigSets(
                default = ['sources', 'start'],
            ),
            sources = InitConfig(
                sources = {
                    "/opt/gameserver/": Join(
                        "",
                        ["https://", Ref(bucket_param), ".s3.amazonaws.com/gameserver/service.zip"])
                },
                files = InitFiles({
                    "/etc/init/gameserver.conf": InitFile(
                        content = Join("", init_service),
                        mode = "000755",
                        owner = "root",
                        group = "root",
                )})),
            start = InitConfig(
                commands = {
                    "a-chown-dir": {
                        "command": "sudo chown -R ubuntu:ubuntu /opt/gameserver",
                    },
                    "b-start-gameserver": {
                        "command" : "sudo service gameserver start",

                    }
                }
            )
        )
    ),
    UserData = Base64(Join("", install_cfn_init)),
    SecurityGroups = [Ref(gameserver_secgroup)],
    InstanceType = Ref(instancetype_param),
    ImageId = FindInMap(
        "AMIPerRegion",
        Ref("AWS::Region"),
        "ami"
    ),
    CreationPolicy = CreationPolicy(
        ResourceSignal = ResourceSignal(
            Timeout = 'PT15M',
            Count = "1",
    ))
))

gameserver_asg = t.add_resource(AutoScalingGroup(
    "GameServerASG",
    DesiredCapacity = Ref(asgscale_param),
    LaunchConfigurationName = Ref(gameserver_config),
    MinSize = 1,
    MaxSize = Ref(asgscale_param),
    LoadBalancerNames = [Ref(gameserver_loadbalancer)],
    AvailabilityZones = GetAZs(""),
    HealthCheckType = "ELB",
    HealthCheckGracePeriod = "300",
    UpdatePolicy = UpdatePolicy(
        AutoScalingRollingUpdate = AutoScalingRollingUpdate(
            PauseTime = 'PT5M',
            MinInstancesInService = "1",
            MaxBatchSize = '1',
            WaitOnResourceSignals = True
    ))
))

gameserver_scaleup = t.add_resource(ScalingPolicy(
    "GameServerScaleUp",
    AdjustmentType = "ChangeInCapacity",
    AutoScalingGroupName = Ref(gameserver_asg),
    Cooldown = "300",
    ScalingAdjustment = "1",
))

gameserver_alarm = t.add_resource(Alarm(
    "BandwidthAlarm",
    AlarmDescription = "Alarm if ingress traffic bandwidth goes above X.",
    Namespace = "AWS/EC2",
    MetricName = "NetworkIn",
    Dimensions = [
        MetricDimension(
            Name = "AutoScalingGroupName",
            Value = Ref(gameserver_asg),
    )],
    Statistic = "Sum",
    Period = "300",
    EvaluationPeriods = "1",
    Threshold = Ref(maxbandwidth_param),
    ComparisonOperator = "GreaterThanThreshold",
    AlarmActions = [Ref(gameserver_scaleup)],
))

gameserver_output = t.add_output([Output(
     "GameServerEndpoint",
     Description = "Endpoint for accessing the service.",
     Value = Join("", ["http://", GetAtt(gameserver_loadbalancer.title, "DNSName"), ":8080"])
)])

utils.validate_template(t.to_json())
print(t.to_json())

